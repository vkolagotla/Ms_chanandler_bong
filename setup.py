#!/usr/bin/python3

import setuptools

with open("README.md", "r") as f:
    unagi_description = f.read()

with open("requirements.txt") as f:
    required = f.read().splitlines()

setuptools.setup(
    name="Ms_chanandler_bong",
    version="0.1.0",
    author="Venkata Kolagotla",
    author_email="vkolagotla@pm.me",
    description="Text generation model based on characters from friends",
    long_description=unagi_description,
    url="https://gitlab.com/vkolagotla/Ms_chanandler_bong.git",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    install_requires=required,
)
