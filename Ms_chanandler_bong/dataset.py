import os
import re
import subprocess
from html.parser import HTMLParser
from re import sub
from sys import stderr
from traceback import print_exc


class _DeHTMLParser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.__text = []

    def handle_data(self, data):
        text = data.strip()
        if len(text) > 0:
            text = sub("[ \t\r\n]+", " ", text)
            self.__text.append(text + " ")

    def handle_starttag(self, tag, attrs):
        if tag == "p":
            self.__text.append("\n\n")
        elif tag == "br":
            self.__text.append("\n")

    def handle_startendtag(self, tag, attrs):
        if tag == "br":
            self.__text.append("\n\n")

    def text(self):
        return "".join(self.__text).strip()


def dehtml(text):
    try:
        parser = _DeHTMLParser()
        parser.feed(text)
        parser.close()
        return parser.text()
    except Exception:
        print_exc(file=stderr)
        return text


def main():
    repo_path = os.path.realpath(__file__)
    repo_path = repo_path.replace("Ms_chanandler_bong/text_filter.py", "")
    file_path = repo_path + "data/html/"
    file_list = os.listdir(file_path)
    for file_name in file_list:
        if file_name.endswith(".html"):

            text = open(file_path + file_name, "r", encoding="ISO-8859-1").read()
            file_name_acc = file_name.replace(".html", "")
            episode_text = dehtml(text)
            episode_text = re.sub("[\(\[].*?[\)\]]", "", episode_text)  # noqa
            episode_text = episode_text.replace("End  '", "")
            episode_text = episode_text.replace("END  '", "")
            episode_text = episode_text.replace("[Time Lapse]", "")
            episode_text = episode_text.replace("[TIME LAPSE]", "")
            episode_text = episode_text.replace("Closing credits", "")
            episode_text = episode_text.replace("MONICA:", "Monica:")
            episode_text = episode_text.replace("MCNA:", "Monica:")
            episode_text = episode_text.replace("MNCA:", "Monica:")
            episode_text = episode_text.replace("JULIE:", "Julie:")
            episode_text = episode_text.replace("JOEY:", "Joey:")
            episode_text = episode_text.replace("PHOEBE:", "Phoebe:")
            episode_text = episode_text.replace("PHOE:", "Phoebe:")
            episode_text = episode_text.replace("ALL:", "All:")
            episode_text = episode_text.replace("GNAG:", "All:")
            episode_text = episode_text.replace("ROSS:", "Ross:")
            episode_text = episode_text.replace("RUSS:", "Russ:")
            episode_text = episode_text.replace("CHAN:", "Chandler:")
            episode_text = episode_text.replace("CHANDLER:", "Chandler:")
            episode_text = episode_text.replace("RACHEL:", "Rachel:")
            episode_text = episode_text.replace("RACH:", "Rachel:")
            text_file = open("{0}.txt".format(file_path + file_name_acc), "w+")
            text_file.write(episode_text)
            subprocess.call(
                ["sed", "-i", "/^[[:space:]]*$/d", file_path + file_name_acc + ".txt"]
            )
            subprocess.call(
                ["sed", "-i", "s/^/> /", file_path + file_name_acc + ".txt"]
            )
            subprocess.call(
                ["sed", "-i", "s/ *$//", file_path + file_name_acc + ".txt"]
            )
            subprocess.call(
                ["sed", "-i", "/^> End/d", file_path + file_name_acc + ".txt"]
            )
            subprocess.call(
                ["sed", "-i", "/^> The One/d", file_path + file_name_acc + ".txt"]
            )
            subprocess.call(
                ["sed", "-i", "/^> Written by/d", file_path + file_name_acc + ".txt"]
            )
            subprocess.call(
                [
                    "sed",
                    "-i",
                    "/^> Transcribed by/d",
                    file_path + file_name_acc + ".txt",
                ]
            )
            subprocess.call(
                [
                    "sed",
                    "-i",
                    "/^> Closing Credits/d",
                    file_path + file_name_acc + ".txt",
                ]
            )
            subprocess.call(
                [
                    "sed",
                    "-i",
                    "/^> Additional transcribing by/d",
                    file_path + file_name_acc + ".txt",
                ]
            )
            subprocess.call(
                [
                    "sed",
                    "-i",
                    "/^> With Minor Adjustments by/d",
                    file_path + file_name_acc + ".txt",
                ]
            )
            subprocess.call(
                [
                    "sed",
                    "-i",
                    "/^> Commercial Break/d",
                    file_path + file_name_acc + ".txt",
                ]
            )
            subprocess.call(
                [
                    "sed",
                    "-i",
                    "/^> COMMERCIAL BREAK/d",
                    file_path + file_name_acc + ".txt",
                ]
            )
            subprocess.call(
                ["sed", "-i", "/^> Produced by/d", file_path + file_name_acc + ".txt"]
            )
            subprocess.call(
                ["sed", "-i", "/^> Friends/d", file_path + file_name_acc + ".txt"]
            )
            subprocess.call(
                ["sed", "-i", "/^> Final check/d", file_path + file_name_acc + ".txt"]
            )
            subprocess.call(
                ["sed", "-i", "/^> 10/d", file_path + file_name_acc + ".txt"]
            )
            subprocess.call(
                ["sed", "-i", "/^> 9/d", file_path + file_name_acc + ".txt"]
            )
            subprocess.call(
                ["sed", "-i", "/^> Directed by/d", file_path + file_name_acc + ".txt"]
            )
            subprocess.call(
                ["sed", "-i", "/^> THE END/d", file_path + file_name_acc + ".txt"]
            )
            subprocess.call(
                ["sed", "-i", "/^> END/d", file_path + file_name_acc + ".txt"]
            )
            subprocess.call(
                ["sed", "-i", "/^> Aired/d", file_path + file_name_acc + ".txt"]
            )
            subprocess.call(
                ["sed", "-i", "/^> Dutch Phrases/d", file_path + file_name_acc + ".txt"]
            )
            subprocess.call(
                [
                    "sed",
                    "-i",
                    "/^> With Help from/d",
                    file_path + file_name_acc + ".txt",
                ]
            )
            subprocess.call(
                ["sed", "-i", "/^> Originally/d", file_path + file_name_acc + ".txt"]
            )
            subprocess.call(
                [
                    "sed",
                    "-i",
                    "/^> Minor additions/d",
                    file_path + file_name_acc + ".txt",
                ]
            )
            subprocess.call(
                [
                    "sed",
                    "-i",
                    "/^> OPENING TITLES/d",
                    file_path + file_name_acc + ".txt",
                ]
            )
            subprocess.call(
                [
                    "sed",
                    "-i",
                    "/^> CLOSING CREDITS/d",
                    file_path + file_name_acc + ".txt",
                ]
            )
            subprocess.call(
                ["sed", "-i", "/^> PART/d", file_path + file_name_acc + ".txt"]
            )
            subprocess.call(
                ["sed", "-i", "/^> Part/d", file_path + file_name_acc + ".txt"]
            )
            subprocess.call(
                ["sed", "-i", "/^> Episodes/d", file_path + file_name_acc + ".txt"]
            )
            subprocess.call(
                [
                    "sed",
                    "-i",
                    "/^> Opening Credits/d",
                    file_path + file_name_acc + ".txt",
                ]
            )
            subprocess.call(
                [
                    "sed",
                    "-i",
                    "/^> With Help From/d",
                    file_path + file_name_acc + ".txt",
                ]
            )
            subprocess.call(
                ["sed", "-i", "/^> .]/d", file_path + file_name_acc + ".txt"]
            )
            subprocess.call(
                ["sed", "-i", "/^> Story by/d", file_path + file_name_acc + ".txt"]
            )
            subprocess.call(
                ["sed", "-i", "/^> Teleplay by/d", file_path + file_name_acc + ".txt"]
            )


if __name__ == "__main__":
    main()
