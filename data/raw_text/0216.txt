> Joey: Man this is weird. You ever realize Captain Crunch's eyebrows are actually on his hat?
> Chandler: That's what's weird? Joey, the man's been captain of a cereal for the last 40 years.
> Chandler: Waaa-aaah.
> Joey: What?
> Chandler: The spoon. You licked and-and you put. You licked and you put.
> Joey: Yeah, so.
> Chandler: Well don't you see how gross that is? I mean that's like you using my toothbrush.  You used my toothbrush?
> Joey: Well, that was only 'cause I used the red one to unclog the drain.
> Chandler: Mine is the red one! Oh God. Can open, worms everywhere.
> Joey: Hey, why can't we use the same toothbrush, but we can use the same soap?
> Chandler: Because soap is soap. It's self-cleaning.
> Joey: Alright, well next time you take a shower, think about the last thing I wash and the first thing you wash.
> Chandler: Hey.
> MONICA and Phoebe: Hey.
> Joey: Hey.
> Phoebe: Ooh, look at you fancy lads. What's the occasion?
> Joey: Well, you know that guy that's on my show that's in a coma? He's havin' a brunch.
> Phoebe: Ahh.
> Rachel:  OK, ready when you are.
> Phoebe: Okey-doke.
> Monica: I can't believe you guys are actually getting tattoos.
> Chandler: Excuse me, you guys are getting tattoos?
> Rachel: Yes, but you can not tell Ross 'cause I want to surprise him.
> Joey: Wow, this is wild. What're you gonna get?
> Phoebe: Um, I'm getting a lily for my Mom. 'Cause her name's Lily.
> Chandler: Wow, that's lucky. What if her name was Big Ugly Splotch?
> Joey: So where you gettin' it?
> Phoebe: I think on my shoulder.
> Ross: What? What's on your shoulder?
> Phoebe: Um, a chip. A tattoo, I'm getting a tattoo.
> Ross: A tattoo? Why, why would you want to do that?  Hi.
> Rachel: Hi. Well hey, you don't - you don't think they're kind of cool?
> Ross: No, sorry I don't. Tell me why would anyone pay someone to scar their body for life? What if it doesn't come out right Phoebe? Then it's like, I don't know, havin' a bad hair cut all the time. Why's everyone staring at me?
> Monica: Ross, come sign this birthday card for dad. Rich is gonna be here any minute.
> Chandler: Oooh, Rich is goin' to the party too, huh?
> Monica: Well, he's my parents' best friend, he has to be there.
> Joey: Oh, is today the day you're gonna tell them about you two?
> Monica: Yeah. It's my dad's birthday, I decided to give him a stroke.
> Phoebe: No, I think you should tell them.
> Monica: No, I don't even know how serious he is about me. Until I do, I'm not telling them anything.
> Ross: I don't know, I don't think mom and dad would mind. Remember when you were 9 and Richard was 30, how dad used to say, 'God I hope they get together.'
> Ross: Alright, shall we?
> Monica: OK, wait, wait, wait, wait. You know what? Ross, let's - let's switch places. You get in the middle. No un-, ya know, unless this looks like we're trying to cover something up.
> Ross: Monica, Monica, you could come in straddling him, they still wouldn't believe it.  We're here.
> MRS. GELLER: Oh hi kids. Hi darling.
> Monica: Happy birthday dad.
> MR. GELLER: Oh thank you.
> Ross: Hi ma.
> RICHARD: Happy birthday.
> MRS. GELLER: Well, you kids thank Dr. Burke for the ride?
> Ross: Uh, actually mom, I think Monica thanked him for the both of us.
> FRIEND: Well, you kids take the train in?
> MRS. GELLER: No, Richard Burke gave them a ride.
> FRIEND: Oh. Speaking of whom, I hear he's got some 20-year-old twinkie in the city.
> Monica: Finger cramp. Oh God, sorry. Here, let me get that mom.
> MRS. GELLER: Sooo, Richard's shopping in the junior section.
> Monica: Are we still on that?
> MRS. GELLER: We just know she's got the IQ of a napkin.
> FRIEND: She's probably not even very pretty, just young enough so that everything is still pointing up.
> Joey: Can you believe this place?
> Chandler: I know, this is a great apartment.
> Joey: Ah, I was just in the bathroom, and there's mirrors on both sides of you. So when you're in there it's like you're peein' with the Rockettes.
> Chandler: Wow, there's my fantasy come true. No, seriously.
> JOEY'S CO-STAR: Hey.
> Joey: Hey! We were just sayin', great apartment man.
> JOEY'S CO-STAR: Thanks. You want it?
> Joey: Huh?
> JOEY'S CO-STAR: Yeah, I'm movin' to a bigger place. You should definitely take this one.
> Joey: Yeah, can you see me in a place like this?
> JOEY'S CO-STAR: Why not? You hate park views and high ceilings? C'mon I'll show you the kitchen.
> Chandler:  Oh that's all right fellas, I saw a kitchen this morning - on TV. Stop talking. OK.
> MR. GELLER: C'mon, tell us.
> FRIEND: Yeah, is she really 20.
> RICHARD: I am not telling you guys anything.
> MR. GELLER: C'mon Rich, it's my birthday, let me live vicariously.
> Ross: Dad, you really don't want to do that.
> MR. GELLER: Ahh, what's a little mid-life crisis between friends?
> RICHARD: Jack, would you let it go?
> MR. GELLER: Look, I know what you're going through. When I turned 50 I got the Porsche. You... you got your own little speedster.
> RICHARD: Guys. Seriously, it is not like that.
> MR. GELLER: Tell you what, maybe one of these weekends you can borrow the car and I cou. . .
> Ross: Dad, I beg you not to finish that sentence.
> MR. GELLER: What? I'm kidding. You know I'd never let him touch the Porsche.
> Phoebe: OK Rach, which, which lily? This lily or that lily?
> Rachel: Well I. . .
> Phoebe: I like this lily. It's more open, ya know, and that's like my mom. She had a more open, giving spirit. Ooh, Foghorn Leghorn, ooh.
> TATTOO ARTIST: Alright, blonde girl, you're in room two, not so blonde girl, you're with me.
> Phoebe: Here we go.
> Rachel:  Uh-huh.
> Phoebe: You're not going?
> Rachel: Uh-huh.
> Phoebe: What? Is it - is this 'cause of what Ross said?
> Rachel: No. Well, yeah, maybe.
> Phoebe: I don't believe this. Is this how this relationship's gonna work? Ross equals boss. I mean, c'mon what is this, 1922?
> Rachel: What's 1922?
> Phoebe: Just, you know, long time ago. Well, when men used to tell women what to do - a lot. And then there was suffrage, which is a good thing but is sounds horrible. Do you want to get this tattoo?
> Rachel: Yes I do, it's just that Ross is. . .
> Phoebe: OK, hey, HEY. Is your boyfriend the boss of you?
> Rachel: No.
> Phoebe: OK, who is the boss of you?!!
> Rachel: You?
> Phoebe: No. You are the boss of you. Now you march your heinie in there and get that heart tattooed on your hip. GO !!
> RICHARD: How ya doin'?
> Monica: I'm a twinkie.
> RICHARD: Really? I'm a hero.
> Monica: Oh, this is so hard.
> RICHARD: Yeah, I know. I hate it too. Look, maybe we should just tell them.
> Monica: Maybe we should just tell your parents first.
> RICHARD: My parents are dead.
> Monica: God, you are so lucky. I mean, I mean. . . you know what I mean.
> RICHARD: I know, I know. Just hang in there, OK. OK, I'll go out first, alright.
> Monica: Alright.
> RICHARD:  Judy, going to the bathroom , good for you.
> MRS. GELLER: Thank you Richard, I appreciate the support.
> MR. GELLER: Honey. Honey, have you seen my Harmon Kilerbrew bat? Bob doesn't believe I have one.
> MRS. GELLER: I have no idea. Did you know Richard has a twinkie in the city?
> MR. GELLER: I know. He's like a new man. It's like a scene from Cocoon .
> MRS. GELLER: I just never would have pictured Richard with a bimbo.
> MR. GELLER: Apparently, he told Johnny Shapiro that she's quite a girl. In fact, he told Johnny that he thinks he's falling in love with her.
> MRS. GELLER: Really.
> MR. GELLER: I tell you, I've never seen him this happy.
> MRS. GELLER: So Jack, you ever think about trading me in for a younger model?
> MR. GELLER: Of course not. With you it's like I've got two 25-year-olds.
> MRS. GELLER:  Oh Jack stop.
> MR. GELLER: C'mon, it's my birthday.
> Joey: Can we drop this? I am not interested in the guy's apartment.
> Chandler: Oh please, I saw the way you were checking out his mouldings. You want it.
> Joey: Why would I want another apartment, huh? I've already got an apartment that I love.
> Chandler: Well it wouldn't kill you to say it once in a while.
> Joey: Alright, you want the truth? I'm thinkin' about it.
> Chandler: What?
> Joey: I'm sorry. I'm 28 years old, I've never lived alone, and I'm finally at a place where I've got enough money that I don't need a roommate anymore.
> Chandler: Woah, woah, woah. I don't need a roommate either, OK? I can afford to live here by myself. Ya know, I may have to bring in somebody once a week to lick the silverware.
> Joey: What're you gettin' so bent out of shape for, huh? It's not like we agreed to live together forever. We're not Bert and Ernie.
> Chandler: Look, you know what? If this is the way you feel, then maybe you should take it.
> Joey: Well that's how I feel.
> Chandler: Well then maybe you should take it.
> Joey: Well then maybe I will.
> Chandler: Fine with me.
> Joey: Great. Then you'll be able to spend more quality time with your real friends, the spoons.
> MR. GELLER: Who's drink can I freshen?
> MRS. GELLER: Almost time for cake.
> Ross: Mon, Mon, are you OK?
> Monica: You remember that video I found of mom and dad?
> Ross: Yeah.
> Monica: Well, I just caught the live show.
> Ross: Eww.
> Monica: Hey there.
> RICHARD: What?
> Monica: Nothing, I just heard something nice about you.
> RICHARD: Humm, really?
> MRS. GELLER: Richard. Richard. Your son isn't seeing anyone is he?
> RICHARD: Uhh, not that I know of.
> MRS. GELLER: Well, I was thinking, why doesn't he give Monica a call?
> RICHARD: That - that's an idea.
> Monica: Well, actually, I'm already seeing someone.
> MRS. GELLER: Oh?
> RICHARD: Oh?
> Ross: Ohh.
> MRS. GELLER: She never tells us anything. Ross, did you know Monica's seeing someone?
> Ross: Mom, there are so many people in my life. Some of them are seeing people and some of them aren't. Is that crystal?
> MRS. GELLER: So, who's the mystery man?
> Monica: Well, uh, he's a doctor.
> MRS. GELLER: A real doctor?
> Monica: No, a doctor of meat. Of course he's a real doctor. And he's handsome, and he's sweet, and know you'd like him.
> MRS. GELLER: Well that's wonderful. . . I
> Monica: Mom, it's OK.
> RICHARD: It is Judy.
> MRS. GELLER: Jack. Could you come in here for a moment? NOW!
> MR. GELLER:  Found it.
> Ross: I'll take that dad.
> MRS. GELLER: It seems your daughter and Richard are something of an item.
> MR. GELLER: That's impossible, he's got a twinkie in the city.
> Monica: Dad, I'm the twinkie.
> MR. GELLER: You're the twinkie?
> RICHARD: She's not a twinkie.
> Monica: Al-alright, l-look you guys, this is the best relationship I've been in. . .
> MRS. GELLER: Oh please, a relationship.
> Monica: Yes, a relationship. For your information I am crazy about this man.
> RICHARD: Really?
> Monica: Yes.
> MR. GELLER: Am I supposed to stand here and listen to this on my birthday?
> Monica: Dad, dad this is a good thing for me. Ya know, and you even said yourself, you've never seen Richard happier.
> MR. GELLER: When did I say that?
> Monica: Upstairs in the bathroom right before you felt up mom.
> Phoebe: Oh that looks so good, oh I love it.
> Rachel: I know, so do I. Oh Phoebe, I'm so glad you made me do this. OK, lemme se yours.
> Phoebe: Ahh. OK, let's see yours again.
> Rachel: Phoebe we just saw mine, let me see yours.
> Phoebe: Oh OK.  Oh no, oh it's gone, that's so weird, I don't know how-where it went.
> Rachel: You didn't get it?
> Phoebe: No.
> Rachel: Why didn't you get it?
> Phoebe: I'm sorry, I'm sorry.
> Rachel: Phoebe, how would you do this to me? This was all your idea.
> Phoebe: I know, I know, and I was gonna get it but then he came in with this needle and uh, di-, did you know they do this with needles?
> Rachel: Really? You don't say, because mine was licked on by kittens.
> Joey: Hey.
> Chandler: Hey.
> Joey: Hey listen, I'm sorry about what happened. . .
> Chandler: Yeah me too.
> Joey: I know. Yeah.
> Chandler: Yeah. So do we need to hug here or. . .
> Joey: No, we're alright.
> Chandler: So I got ya something.
> Joey: Plastic spoons. Great.
> Chandler: Lick away my man.
> Joey: These'll go great in my new place. You know, 'till I get real ones.
> Chandler: What?
> Joey: Well, I can't use these forever. I mean, let's face it, they're no friend to the environment.
> Chandler: No-no, I mean what, what's this about your new place?
> Joey: I'm movin' out like we talked about.
> Chandler: Well I didn't think that was serious.  Ya know I thought that was just a fight.
> Joey: Well, it was a fight. . . based on serious stuff, remember. About how I never lived alone or anything. I just think it would be good for me, ya know, help me to grow or. . . whatever.
> Chandler: Well, there you go.
> Joey: Hey, are you cool with this. I mean, I don't want to leave you high and dry.
> Chandler: Hey, no, I've never been lower or wetter. I'll be fine. I'll just turn your, uh, bedroom into a game room or somethin', you know, put the foosball table in there.
> Joey: Woah. Why do you get to keep the table?
> Chandler: I did pay for half of it.
> Joey: Yeah. And uh, I paid for the other half.
> Chandler: Alright I'll tell you what, I'll play you for it.
> Joey: Alright, you're on. I can take two minutes out of my day to kick your ass.
> Chandler: Your little men are gonna get scored on more times than your sister.
> Joey: Woah, woah, woah, woah. Which sister?
> Monica: So, are you sorry that I told them?
> RICHARD: No, it's been a long time since your dad and I went running.
> Rachel: Oh.
> Monica: Oh. Well did you get it? Let me see.
> Rachel: Is Ross here?
> Monica: No he went out to get pizza.
> Rachel: Oh really, OK.
> Monica: That's great.
> RICHARD: Very tasteful.
> Phoebe: Wanna see mine, wanna see mine?
> Monica: Yes.
> Rachel: What? You didn't get one.
> Phoebe: OK, well then what is this?
> RICHARD: What're we looking at? That blue freckle?
> Phoebe: OK, that's my tattoo.
> Rachel: That is not a tattoo, that is a nothing. I finally got her back in the chair, bairly touched her with a needle, she jumped up screaming, and that was it.
> Phoebe: OK, hi. For your information this is exactly what I wanted. This is a tattoo of the earth as seen from a great distance. It's the way my mother sees me from heaven.
> Rachel: Oh, what a load of crap. That is a dot. Your mother is up in heaven going, 'Where the hell is my lily, you wuss?' OK, Phoebe, that is not a tattoo, this is a tattoo.
> Ross: You got a tattoo?
> Rachel: Maybe. But just a little one. Phoebe got the whole world.
> Ross: Lemme see.
> Rachel: Well?
> Ross: Well it's really. . . sexy. I wouldn't have thought it would be but. . . wow.
> Rachel: Really?
> Ross: Yeah, so uh, is it sore or can you do stuff?
> Rachel: I guess.
> Ross: Hey, save us some pizza.
> Joey: Get out of the corner. Pass it, pass it.
> Chandler: Stop talkin' to your men.
> Joey: Yes! And the table is mine.
> Chandler: Congratulations.
> Joey: Hey, you guys are still gonna come visit me, right?
> Chandler: Oh yeah, you got the big TV. We'll be over there all the time. . .  except when we are here.
> Phoebe: I know you're just moving uptown but I'm really gonna miss you.
> Monica: I know, how can you not be accross the hall anymore.
> Rachel: Yeah, who's gonna eat all our food, and tie up our phone lines, and - is that my bra? What the hell you doin' with my bra?
> Joey: Oh no-no, it's uh, it's not what you think. We uh, we used it to, you know, fling water balloons off the roof. Remember that, those junior high kids couldn't even get theirs accross the street.
> Chandler:  Yeah, I remember.
> Ross: Hey, let's bring the rest of these down to the truck.
> Chandler: So, uhh, em, you want me to uh, give you a hand with the foosball table?
> Joey: Naa, you keep it, you need the practice.
> Chandler: Thanks.
> Joey: So, I guess this is it.
> Chandler: Yeah, right, yeah, I guess so.
> Joey: Listen, uh, I don't know when I'm gonna see you again.
> Chandler: Well, I'm guessing uh, tonight at the coffee house.
> Joey: Right, yeah. OK. Um, take care.
> Chandler: Yeah.
