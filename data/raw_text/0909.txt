> JO LYNN: This kitty is Mittens and this one is Fitzhugh, and this little guy in the cat condo is Jinkies.
> Chandler: Yep.  That's a lot of cats Jo Lynn.  Single are ya?
> Chandler: Chandler Bing.
> Joey: Hey.  How come you're answering your own phone?  Where's your crazy assistant?
> Chandler: What's up Joe?
> Joey: Okay, what have we always wanted to do together?
> Chandler: Braid each other's hair and ride horseback on the beach?
> Joey: No, no, no.  When you get home tomorrow night, you and I are going to be at the Wizzards-Knicks game . . .  courtside!
> Chandler: Courtside?  Oh my God.
> Joey: Yeah.  Maybe Michael Jordon will dive for the ball and break my jaw with his knee.
> Chandler: That's so cool.  I'll let Monica know.
> Monica: Hello?
> Chandler: Joey just called.  He's got courtside Knicks tickets for him and me tomorrow night.
> Monica: Really?  But tomorrow night is the only night I get off from the restaurant.  If you go to the game, we won't have a night together for another week.
> Chandler: But hey, it's courtside.  The cheerleaders are going to be right in fr. . .   That's not the way to convince you.
> Monica: Chandler look, I don't want to be one of those wives who says, "You can't go to the game.  You have to spend time with me."  So, if you could just realize it on your own . . .
> Chandler: I know.  You're right.  I want to see you too.  I've just got to figure out a way to tell Joey, you know?  He's really looking forward to this.
> Monica: Tell him that you haven't seen your wife in a long time.  Tell him that having a long-distance relationship is really difficult.  Tell him that what little time we have is precious.
> Chandler: Yeah, ah, ah . . .  I'll think of something.
> Ross: Wow!    Wow, You look . . . uh . . .   It's just, ah . . .   That dress . . . uh . . .
> Rachel: Well, I hope the ends of these sentences are good.
> Ross: Well, well, they're good.  It's been a while since I've seen you like this.  You, you clean up good.
> Rachel: Oh well, well thank you.    Okay, stop.  Stop looking at me like that.  The last time that happened,  that happened. 
> Ross: Oh right, right.    So, are you . . . ah . . . you excited about your, your first night away from Emma?
> Rachel: Yeah, yeah.  Phoebe and I are going to have so much fun.  And thank you for watching the baby, by the way.
> Ross: Oh, it's fine.  Actually, I, I invited Mike over.
> Rachel: Phoebe's Mike?
> Ross: Yeah.
> Rachel: I didn't know you guys hung out.
> Ross: We don't.  But I thought it would be nice to get to know him.  You know, maybe have a little dinner, drinks, conversation.
> Rachel: Oh that's so cute:  Ross and Mike's first date.  Is that going to be awkward?  I mean, what are you guys going to talk about?
> Ross: I don't know.  But, you know, we, we have a lot in common, you know.  He plays piano; I played keyboards in college.  He's been divorced; I have some experience in that area.
> Rachel: Yeah.
> Phoebe: Hi
> Rachel: Hey.
> Phoebe:  Oooh.  Girl's night out indeed.
> Rachel:  Ok.  So now, I think Emma is probably down for the night, but if you need anything Ross . . .
> Ross: Rach, Rach, we'll be fine, all right?  You go have fun.
> Rachel: Okay.  You too.  And I hope you score.   Bye.
> MIKE: Bye.
> Ross: So . . .  Welcome.
> MIKE:  I got beer.
> Ross: I got bottled breast milk.
> MIKE: Eh, why don't we start with the beer?
> Ross: Okay.    So, um, Phoebe tells me you, ah, you play piano.
> MIKE: Yeah.
> Ross: You know, I, I used to, ah, play keyboards in college.
> MIKE: Ah?    Do you have one here?
> Ross: No.
> MIKE: Okay. 
> Ross: Um . . . ah . . . you know, I'm divorced.  Um, Phoebe, ah . . . Phoebe said you . . . You've been divorced?
> MIKE: Yeah.    Yeah, I'm sorry.  I don't . . . I don't really like to talk about it.
> Ross:  That's okay.  We'll talk about  something else. 
> MIKE:  So, you're a paleontologist, right?
> Ross: Yeah.
> MIKE: My cousin's a paleontologist.
> Ross: Ah?    Well, he and I would probably have a lot to talk about.
> Monica: Hi.
> Chandler: Hey.
> Monica: Welcome home.
> Chandler:   Oh well, look at you.
> Monica: Yeah.  What do you think?
> Chandler: Well, it looks great.  It's just that . . . well, I'm wearing the same thing underneath.  So . . .
> Monica: Oh.
> Chandler: See what I mean . . .
> Joey: Hey!  How come the door's locked?
> Monica: Just a second.
> Chandler:  No, no, no, no, no.  Joey can't know that I'm here.
> Monica: Why not?
> Chandler: Because I didn't know how to tell him that I couldn't go to the Knicks game.  So, I just told him that I had to stay in Tulsa.
> Monica: So, you lied to him?
> Chandler: Achhh.  It's always better to lie than to have the complicated discussion.    Except with you.
> Joey: Hey!  Open the door.  What's going on? 
> Joey: What are you . ..    Why are you dressed like that?
> Monica: Oh, because, um . . .  well, Chandler's going to be home in a couple of days.  So, I thought I would, you know, practice the art of seduction.
> Joey: Oh, I thought I heard a man's voice before.
> Monica: Oh I was just doing Chandler's side of the conversation.  You know, like, "Hi, How do I look?"   "Really sexy.  Could I BE any more turned on?"
> Joey: Okay.    Whoa, whoa.  Why are there two glasses of wine out?
> Monica: Because. . .  one of them is for you.
> Monica: Cheers.    Okay, buh-bye. 
> Chandler: You know, it's funny.  I've been, ah, practicing the art of seduction myself.    Hi ya.
> Monica: You might want to keep practicing.
> Chandler: Yah.
> Chandler: It's Joey.    Hey Joe.
> Joey: Dude, come home!
> Chandler: What? Why?
> Joey: COME . . .  HOME.
> Chandler: Look I, I can't.  What's going on?
> Joey: I don't know how to tell you this but, uh . . . I think Monica's cheatin' on ya.  I told you shouldn't have married someone so much hotter than you.
> Joey: All right look.    If you can't come home and deal with this, then I'm gonna.
> Chandler: NO!
> Joey:  I just heard him!
> Chandler:  Can you . . . hear him . . . now?
> Joey:   No.   All right, I'm going in.
> Chandler: No! Wait!
> Joey: I heard him again!
> Chandler:  All right, look.  Just stay there.  I'm coming home.
> Joey: Okay.  Great.  I'll see you when you get here.  I'm gonna wait out in the hall in case the dude comes out.
> Chandler: Is that really necessary?
> Joey: Absolutely.  You'd do it for me.  Not that you ever have to because I know how to keep my women satisfied.
> Ross: Shouldn't the pizza be here by now?  I mean, they said thirty minutes or less.  Well, how long has it been?
> MIKE:  Eleven minutes.    And now twelve.  So, do you like the beer?
> Ross: I do.  I do.  Although, it's actually a lager.
> MIKE: huh.    What's the difference between beer and lager?
> Ross: I don't know.  We could look it up.
> MIKE:  Things are about to get wild.
> Phoebe: Oh God.  Remember the girls' nights we used to have sitting around talking about you and Ross?
> Rachel: Oh God.  It seems like forever ago.
> Phoebe: I know.     So, what's going on with you and Ross?
> Rachel: Well, um . . . I don't know.  I mean, for a long time nothing.  But you know, actually right before you picked me up, Ross and I had a . . . ah . . . little thing.
> Phoebe: Oh my God!  I love things.  What happened?
> Rachel: Well, um, first he told me he liked how I looked.  And, ah, then we had a little . . . um . . . eye-contact.
> Phoebe: Eye-contact?
> Rachel: Mm-hmm.
> Phoebe: I hope you were using protection.
> WAITER:   Excuse me.  Um, these are from the two gentlemen at the end of the bar.
> Rachel: Oh.   Should we send them something back?
> Phoebe: Oooh.  Let's do.  Let's send them mashed potatoes.
> Rachel: No!  Wait!  No, no.  Don't do that!  That's going to make them think they can come over here.
> Phoebe: So?  What if they do?
> Rachel: Well, we're not here to meet guys.  You have a boyfriend, I have a b. . . baby and a Ross.
> Phoebe: Yeah, but, ah, ah, nothing has to happen.  We're just having fun.  You know, not everything had to go as far as "eye-contact."
> Monica: Chandler, you have to tell Joey that you're not in Tulsa.
> Chandler: Don't you think it's better for him to think that you're cheating on me, than for him to think that I'm cheating on him?    I heard it.
> Monica: I don't want him to think that I'm having an affair.
> Chandler: All right.  I've got a plan.  I'll go down the fire escape.
> Monica: Yes, because all good plans start with, "I'll go down the fire escape."
> Chandler: Hear me out woman.  I'll go down the fire escape.  Then, I'll wait for a while.  Then, when I come up the stairs, it'll be just like I just got back from Tulsa.  Then, Joey and I will come in and see that there's no guy in here.
> Monica: Aren't you afraid that Joey's going to figure all of this out?    I heard it.
> Chandler: I'm just going to wait for a little while.
> Monica: Scary pigeon's back?
> Chandler: It's huge.
> Rachel: Oh my God.  I can't believe you live in that building.  My grandmother lives in that building.  Ida Green?  No sense of personal space?  Kind of smells like chicken?  Looks like a potato.
> BILL: "Spuds" is your grandmother?
> Rachel: That's my bubby!
> KEVIN: So, we're on our way to a couple of parties.  Um. . . maybe we can get your numbers and give you guys a call if we find something fun.
> Phoebe: Yeah. . . I'm sorry.  We weren't really looking for anything to happen with you guys.  I, I have a boyfriend.
> KEVIN: All right. It's no big deal.
> BILL: So, she has a boyfriend. What is your situation?
> Rachel: Oh, well, it's complicated. I don't actually have a boyfriend.  But um. . .
> BILL: Then, can I have your number?
> Rachel:  I'm sorry, no.
> BILL: Okay.
> Rachel: Oh sure. 
> Phoebe:  Oh my God, you're giving your real number.
> BILL: Okay, thanks. I'll give you a call later tonight.
> Rachel: Great.
> BILL: Bye
> Phoebe: Bye.  Wow. So, that's great. You, Bill, Ross, and Emma are going to be so happy together. What were you thinking?
> Rachel: I don't know.  He was cute, and he liked me.  It was an impulse.
> Phoebe: What about Ross?  What about your moment?  Don't you want to talk to Ross about it?
> Rachel: No.  No, because I know exactly how the conversation's gonna go.  "Hey Ross, you know, I think we had a moment before."
> Rachel:  "Yeah."    "Me too."
> Rachel:  "Well, but I'm not sure I really want to do anything about it."
> Rachel:  "Yeah."   "Me neither."
> Rachel:  "Well, should we just continue to live together and not really tell each other how we're really feeling?"
> Rachel:  "Yeah.  That works for me."
> Phoebe: Yeah, I see what you mean.  By the way, nice Ross imitation.
> Rachel: Oh, thanks.
> Phoebe: But, your Rachel wasn't whiny enough.
> Rachel:  Wha. . . hey!
> Phoebe:  Better!
> Rachel: Well, the point is, maybe I should just stop waiting around for moments with Ross, you know?  I should just . . . move on with my life.
> Phoebe: Really?  You're moving on from Ross?
> Rachel: I don't know.  Do I have to decide right now?
> Phoebe: Well, you kind of just did.  That guy is going to call you tonight.  Ross is going to pick up the phone and that's a pretty clear message.
> Rachel: Oh God, Ross.  Ross is going to pick up the phone.  Oh, I have to get my number back.    Oh my God.  He's gone.
> Phoebe:  "Oh, I have to get my number back.  Oh my God.  He's gone."   Dead on.
> MIKE:  Ya know, I'm going to take off.
> Ross: So soon?
> MIKE: Well, yeah.
> Ross: Okay.  Well, thanks, ah, thanks for the beer.
> MIKE: Ah, you mean lager.
> Ross: Ah yeah.  Good times.
> MIKE:  Oh.
> MIKE: Hello?
> Phoebe:  Hey, Mike, it's me.  Listen, is um, is Ross near you?
> MIKE: Uh, no.  I just left.
> Phoebe: Well, you have to go back in.
> MIKE: Wha . . .?  Go back?  To the "land where time stands still"?
> Phoebe: I'm so sorry honey, but, okay, Rachel gave this guy her number and, um, she doesn't want Ross to answer the phone.  So, you have to intercept all his calls.
> MIKE: I can't do that!
> Phoebe:  He says he can't do that.
> Rachel: Oh give me , , ,    Hi, Mike?  Hi.  Listen.  I know this is a lot to ask, but you know what?  If you do this I . . . Phoebe will . . . do anything you want.  Seriously, I'm talking dirty stuff.
> Phoebe: All right.   Hello?  Hi.  I'm sorry about her, but she wasn't wrong about the dirty stuff.
> MIKE: All right.  I'll do it.    But really, how much dirtier can it get?
> Phoebe:  Oh, Mike.  Bye.
> MIKE: Hey buddy.
> Ross: Uh, hi.
> MIKE: Um, can I come back in?
> Ross:   Why? 
> MIKE:  I, I was just thinking about how much more we have to talk about.
> Ross:  But you left.
> Joey: Wow!  That didn't take long.  I thought you said Tulsa was, like a three hour flight.
> Chandler:  Well, you're forgetting about the time difference.
> Monica:  Chandler!  You're home!
> Chandler: That's right.  You're husband's home.  So, now the sex can stop.
> Monica: What are you saying?
> Chandler: Joey said that you're in here with another man.
> Monica: There's no man in here.  How dare you accuse me of that. 
> Joey: All right.  All right.  Then, maybe you won't mind if me and my friend take a look around, huh?    Bwa-ah-ah!
> Chandler:  What is he doing?
> Monica:  I arranged some pillows on the bed to look like a guy.
> Joey:  Bedroom is clear, although you might need some new pillows.
> Chandler: All right.  Well, I'll check the guest room.
> Joey:   Why do I smell men's cologne?
> Monica:  I think that's you.
> Joey:   Oh yeah.  I rubbed a magazine on myself earlier.
> Chandler: There's nobody here Joe.
> Joey: I guess not.
> Monica: I can't believe you thought I was cheating.   You own me an apology.
> Joey: Yeah, right Monica.  I'm so sorry.
> Monica:  Ah, it's an honest mistake.  It could happen to anyone.  All right, see ya.
> Joey:  Whoa, whoa.  Wait a minute.  Wait a minute.  If you just got back from Tulsa, how did your suitcase beat you here?
> Chandler:  I climb down the fire escape and you can't put that in the closet?
> MIKE: So, except for the fermentation process, beer and ale are basically the same thing.  Fascinating isn't it.
> Ross: Maybe you should look up "fascinating."
> MIKE: I'll get it.    Hello?  Ross's place.  Mike speaking.    It's for you.
> Ross:  I don't understand what just happened here.
> Joey: What's going on?
> Chandler: I'm sorry.  I, I told you I was in Tulsa because I wanted to spend the night with Monica and I, I didn't know . . .  I didn't think you'd understand.
> Joey: What? You think I'm too dumb to understand that a husband needs to be with his wife?  Huh?  Do you think I'm like, "Duh." 
> Monica: Joey?
> Joey: Yeah? 
> Monica: I don't know what to say.  We shouldn't have lied to you.
> Chandler: Yeah.  I feel so bad.  Is there anything I can do to make it up to you?
> Joey:  Yeah, you could go to the game with me, ah, even though I know you said you couldn't.  But then you lied to me and tricked me and gave me a bump on the head.
> Chandler: I'm sorry.  That's the one thing I can't do.  I promised I'd be with Monica.
> Joey: All right.
> Monica:  You can go.
> Chandler: What?
> Monica: You should go to the game.  It's okay.  I want you to.
> Chandler: Really?  You're gonna be okay?
> Monica: Yeah, I'll be fine.  You know, maybe I'll stay here and practice the art of seduction.
> Chandler: You're gonna put on sweats and clean, aren't you?
> Monica: It's gonna be so hot! 
> Chandler: Okay, bye.
> Monica: Have fun.
> Joey: Thanks.    Here's your ticket.
> Chandler: Hey, listen.  I'm never going to lie to you again, okay?  And I want you to know that nobody thinks you're stupid.
> Joey: Thanks man.
> Joey: Where are you going?
> Chandler: Game's tomorrow night Joe.
> Rachel: Hi.
> Ross: Oh God. 
> Rachel: Oh . . .
> MIKE: I'm so glad you're back. 
> Phoebe: Oh.
> Rachel: Wow.  So, what did you guys do?
> Ross: Oh, you know . . . we just drank some beer and Mike played with the boundaries of normal social conduct.
> MIKE: It's true.  I did.
> Phoebe:  Well, good bye.
> Rachel: That was fun Pheebs.
> Phoebe: I know.  That was fun. 
> Rachel: See you guys. 
> Ross: Rachel, lock the door.  Lock the door,  seriously.
> Rachel: Oh shoot.  I forgot to pay Phoebe for the drinks.    Wait, wait.  Sorry.  Did he call?  Did that guy call?
> MIKE: No.  Just his mom.
> Rachel: Oh, around 8:30?
> MIKE: Yeah.
> Rachel: Then, again at 9:00?
> MIKE:  uh-huh.
> Rachel: Yeah.
> Ross: Hello.    Ah, no, she's not here right now.  Can I take a message?    Bill from the bar?    Okay, "Bill from the bar."  I'll make sure she gets your number.
> Rachel: Aaah. 
> Ross: So, ah . . . So, how was it?   Uh, did you guys. . . Did you guys have a good time?
> Rachel: Oh, it was so much fun.  It felt so good to be out.
> Ross:  Uh, Rach.
> Rachel:  Yeah?
> Ross:   Never mind.
> Ross: Hey you guys. 
> MIKE: Hey.
> Phoebe: Hey.  I'll be right back.  I've got to go to the bathroom. 
> MIKE:  Stout.  That's a kind of beer.Ross smiles slightly.  Then he gives a single nod that lifts him to his feet.  He exits the coffee shop.
