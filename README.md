# Ms_chanandler_bong

Text generation model based on characters from friends. The model is not trained yet since i do not have access to a GPU as of now (Looking into google colab).

## Installation
Installing Ms. Chanandler Bing is easy. Clone the repo and install it with python setuptools.
```bash
   $  git clone https://gitlab.com/vkolagotla/Ms_chanandler_bong.git
   $  cd Ms_chanandler_bong
   $  python3 setup.py install
```

## Prepare train data
The **input.txt** file in the data/raw_text folder is the actual train text from all the episodes. If you want to prepare the train data from scratch for whatever reason, clone the following [repo](https://github.com/fangj/friends.git) to get the original html files for each episode and start scraping the text from them.
```bash
   $  git clone https://github.com/fangj/friends.git
   $  mv season/ data/html/
```
The train data file(input.txt) can be recreated by..
```bash
   $  python3 Ms_chanandler_bong/dataset.py
   $  cd Ms_chanandler_bong/data/raw_text/
   $  ls *.txt | xargs -L 1 cat >> input.txt
```

## Future work
1. Add docstrings and host documentation
2. Avoid running scripts directly
3. Add typehinting
4. Add trained model

## Thanks to
1. [Pender](https://github.com/pender) - for making [chatbot-rnn](https://github.com/pender/chatbot-rnn) available to everyone.
2. [Fangj](https://github.com/fangj) - for making [friends](https://github.com/fangj/friends) transcripts available to everyone.
